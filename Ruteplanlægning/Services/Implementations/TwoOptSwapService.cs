﻿using System.Collections.Generic;
using System.Linq;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services.Implementations
{
    public class TwoOptSwapService : IAlgorithmService
    {
        private List<int> tour;
        private Graph _graph;
        private List<int> NewTour { get; set; } = new List<int>();

        public List<int> RunAlgorithm(Graph graph)
        {
            _graph = graph;

            //Running NNA to get tour to optimize
            var nnaServioce = new NearestNeighbourService();
            tour = nnaServioce.RunAlgorithm(graph);

            return TwoOpt();
        }

        private List<int> TwoOpt()
        {
            var best = SharedMethods.TourLength(_graph, tour);

            var nodes = _graph.Nodes.Count;
            var improve = 0;

            var newTour = tour.ToList();
            while (improve < 500)
            {
                for (var i = 1; i < nodes - 1; i++)
                {
                    for (var k = i + 1; k < nodes; k++)
                    {
                        var newerTour = Swap(i, k);

                        var newDistance = SharedMethods.TourLength(_graph, newerTour);

                        if (!(newDistance < best)) continue;

                        improve = 0;
                        best = newDistance;
                        newTour = newerTour;
                        tour = newerTour;
                    }
                }
                improve++;
            }

            return newTour;
        }

        private List<int> Swap(int i, int j)
        {
            //check to ensure start and end are never swapped
            if (i == 0 || j == tour.Count - 1)
            {
                return tour;
            }

            var size = tour.Count;
            var newerTour = new int[tour.Count];

            // 1. take tour[0] to tour[i-1] and add them in order to new_route
            for (var c = 0; c <= i - 1; c++)
            {
                newerTour[c] = tour[c];
            }

            // 2. take tour[i] to tour[j] and add them in reverse order to new_route
            var change = 0;
            for (var d = i; d <= j; d++)
            {
                newerTour[d] = tour[j - change];
                change++;
            }

            // 3. take tour[k+1] to end and add them in order to new_route
            for (var e = j + 1; e < size; e++)
            {
                newerTour[e] = tour[e];
            }

            return newerTour.ToList();
        }
    }
}
