﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruteplanlægning.Services.ServiceClasses
{
    public class Node
    {
        public readonly int Id;
        public string Name { get; set; }
        public bool Flag { get; set; } // Used by various algorithms
        public bool SecondaryFlag { get; set; } // Used by various algorithms

        public Node(int id)
        {
            Id = id;
        }
    }
}
