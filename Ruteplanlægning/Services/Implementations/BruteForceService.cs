﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services.Implementations
{
    public class BruteForceService : IAlgorithmService
    {
        public List<int> RunAlgorithm(Graph graph)
        {
            //Validation that the graph is not too big
            if (graph.Nodes.Count > 10)
            {
                throw new ArgumentOutOfRangeException(nameof(graph));
            }

            var home = graph.Nodes[0];
            graph.Nodes.Remove(home);

            var nodesId = graph.Nodes.Select(x => x.Id).ToList();
            var allPermutations = GetPermutations(nodesId, nodesId.Count).ToList();

            double bestTourWeight = Double.MaxValue;
            var bestTour = new List<int>();

            graph.Nodes.Add(home);

            foreach (var permutation in allPermutations)
            {
                //Adding home as the stop for every permutation
                var permutationAsList = permutation.ToList();
                permutationAsList.Add(home.Id);
                permutationAsList.Add(home.Id);

                SharedMethods.ShiftRight(permutationAsList, 1); //shifts list right, causing the home node to be both start and end

                var permutationLength = SharedMethods.TourLength(graph, permutationAsList);
                if (!(permutationLength < bestTourWeight)) continue;
                bestTourWeight = permutationLength;
                bestTour = permutationAsList;
            }

            return bestTour;
        }

        private static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new[] { t });
            return GetPermutations(list, length - 1).SelectMany(t => list.Where(e => !t.Contains(e)), (t1, t2) => t1.Concat(new[] { t2 }));
        }
    }
}
