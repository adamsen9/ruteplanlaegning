﻿using System;
using System.Collections.ObjectModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ruteplanlægning.Model;
using Ruteplanlægning.Services;

namespace Ruteplanlægning.Tests
{
    [TestClass]
    public class RouteOptimizationServiceTests
    {
        [TestMethod]
        public void OptimizeRoute_Returns_Nna()
        {
            //Arrange
            var route = new ObservableCollection<RouteStopObject>
            {
                new RouteStopObject {Name = "Roskilde", Longitude = 12.087845, Lattitude = 55.64191, IsHome = true},
                new RouteStopObject {Name = "København", Longitude = 12.56834, Lattitude = 55.6761},
                new RouteStopObject {Name = "Holbæk", Longitude = 11.71758, Lattitude = 55.71466},
                new RouteStopObject {Name = "Helsingør", Longitude = 12.59213, Lattitude = 56.03079},
                new RouteStopObject {Name = "Glostrup", Longitude = 12.40214, Lattitude = 55.66922}
            };

            //Act
            var result = RouteOptimizationService.OptimizeRoute(route, AlgorithmEnum.Nna, out var output);

            //Assert
            Assert.IsTrue(result is ObservableCollection<RouteStopObject>);
            Assert.IsTrue(result != null);

            Assert.AreEqual(result.Count, route.Count + 1);

            Assert.IsTrue(result[0].Name == route[0].Name);

            Assert.IsTrue(result[result.Count - 1].Name == route[0].Name);
        }


        [TestMethod]
        public void OptimizeRoute_Returns_BruteForce()
        {
            //Arrange
            var route = new ObservableCollection<RouteStopObject>
            {
                new RouteStopObject {Name = "Roskilde", Longitude = 12.087845, Lattitude = 55.64191, IsHome = true},
                new RouteStopObject {Name = "København", Longitude = 12.56834, Lattitude = 55.6761},
                new RouteStopObject {Name = "Holbæk", Longitude = 11.71758, Lattitude = 55.71466},
                new RouteStopObject {Name = "Helsingør", Longitude = 12.59213, Lattitude = 56.03079},
                new RouteStopObject {Name = "Glostrup", Longitude = 12.40214, Lattitude = 55.66922}
            };

            //Act
            var result = RouteOptimizationService.OptimizeRoute(route, AlgorithmEnum.BruteForce, out var output);

            //Assert
            Assert.IsTrue(result is ObservableCollection<RouteStopObject>);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public void OptimizeRoute_Returns_TwoOptSwap()
        {
            //Arrange
            var route = new ObservableCollection<RouteStopObject>
            {
                new RouteStopObject {Name = "Roskilde", Longitude = 12.087845, Lattitude = 55.64191, IsHome = true},
                new RouteStopObject {Name = "København", Longitude = 12.56834, Lattitude = 55.6761},
                new RouteStopObject {Name = "Holbæk", Longitude = 11.71758, Lattitude = 55.71466},
                new RouteStopObject {Name = "Helsingør", Longitude = 12.59213, Lattitude = 56.03079},
                new RouteStopObject {Name = "Glostrup", Longitude = 12.40214, Lattitude = 55.66922}
            };

            //Act
            var result = RouteOptimizationService.OptimizeRoute(route, AlgorithmEnum.TwoOpt, out var output);

            //Assert
            Assert.IsTrue(result is ObservableCollection<RouteStopObject>);
            Assert.IsTrue(result != null);
        }

        [TestMethod]
        public void OptimizeRoute_Returns_Christofides()
        {
            //Arrange
            var route = new ObservableCollection<RouteStopObject>
            {
                new RouteStopObject {Name = "Roskilde", Longitude = 12.087845, Lattitude = 55.64191, IsHome = true},
                new RouteStopObject {Name = "Glostrup", Longitude = 12.40214, Lattitude = 55.66922},
                new RouteStopObject {Name = "København", Longitude = 12.56834, Lattitude = 55.6761},
                new RouteStopObject {Name = "Holbæk", Longitude = 11.71758, Lattitude = 55.71466},
                new RouteStopObject {Name = "Køge", Longitude = 12.1821809, Lattitude = 55.457526}
            };

            //Act
            var result = RouteOptimizationService.OptimizeRoute(route, AlgorithmEnum.Christofides, out var output);

            //Assert
            Assert.IsTrue(result is ObservableCollection<RouteStopObject>);
            Assert.IsTrue(result != null);
        }
    }
}
