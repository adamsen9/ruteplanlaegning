﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Maps.MapControl.WPF;
using Ruteplanlægning.Model;
using Ruteplanlægning.Services;

namespace Ruteplanlægning
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly Map _myMap;
        private readonly TextBox _addressField;

        private readonly TextBlock _optimizedLengthField;
        private readonly TextBlock _displayedLocation;

        private readonly ObservableCollection<RouteStopObject> _listOfLocationsOnRoute;

        private readonly ObservableCollection<OptimizedRouteName> _optimizedRoute;

        private Location _addressAsLocation;

        private readonly ComboBox _algorithmDropdown;

        public MainWindow()
        {
            InitializeComponent();
            //Setting up references to XAML controls
            _myMap = MyMap;
            _addressField = AddressField;
            _algorithmDropdown = AlgorithmDropdown;
            _optimizedLengthField = LengthResult;

            _displayedLocation = DisplayedLocation;

            // Button setup
            SeachForCoordinates.Click += GetAddressAsLocationClick;
            OptimizeRouteButton.Click += OptimizeRoute;
            AddToRouteButton.Click += AddLocationToRoute;

            //Menu item setup
            ClearRouteItem.Click += ClearRoute;
            ExitMenuItem.Click += Exit;


            //Binding of locations to list
            _listOfLocationsOnRoute = new ObservableCollection<RouteStopObject>();
            _optimizedRoute = new ObservableCollection<OptimizedRouteName>();

            RouteLocationsList.ItemsSource = _listOfLocationsOnRoute;
            OptimizedRouteList.ItemsSource = _optimizedRoute;

            _algorithmDropdown.ItemsSource = Enum.GetValues(typeof(AlgorithmEnum));
        }

        private void OptimizeRoute(object sender, RoutedEventArgs e)
        {
            if (_listOfLocationsOnRoute.Count == 0)
            {
                return; //empty list of locations, nothing to optimize on
            }

            AlgorithmEnum pickedEnum;
            try
            {
                Enum.TryParse(_algorithmDropdown.Text, out pickedEnum);
            }
            catch (Exception)
            {
                return;
            }

            var results = RouteOptimizationService.OptimizeRoute(_listOfLocationsOnRoute, pickedEnum, out var lengthResult);
            _optimizedLengthField.Text = "Længde: " + Math.Round(lengthResult / 1000, 2) + " km.";

            _optimizedRoute.Clear();

            foreach (var routeStopObject in results)
            {
                _optimizedRoute.Add(new OptimizedRouteName { Name = routeStopObject.Name });
            }

            //More than one stop was returned
            _optimizedRoute[0].Name = "Start: " + _optimizedRoute[0].Name;
            _optimizedRoute[_optimizedRoute.Count - 1].Name = "Slut: " + _optimizedRoute[_optimizedRoute.Count - 1].Name;

            for (var i = 1; i < _optimizedRoute.Count - 1; i++)
            {
                var stop = _optimizedRoute[i];
                stop.Name = "" + i + ": " + stop.Name;
            }


        }

        private void GetAddressAsLocationClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var enteredString = _addressField.Text;
                _addressAsLocation = GetPointFromAddress(enteredString);

                _displayedLocation.Text = " " + enteredString;

                _myMap.Children.Clear();
                var pin = new Pushpin { Location = _addressAsLocation };
                _myMap.Children.Add(pin);
                _myMap.Center = _addressAsLocation;
                _myMap.ZoomLevel = 12;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void AddLocationToRoute(object sender, RoutedEventArgs e)
        {
            try
            {
                var newItem = new RouteStopObject
                {
                    Name = _displayedLocation.Text,
                    Longitude = _addressAsLocation.Longitude,
                    Lattitude = _addressAsLocation.Latitude
                };

                _listOfLocationsOnRoute.Add(newItem);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void TextBox_KeyEnterUpdate(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                GetAddressAsLocationClick(sender, e);
            }
        }

        private static Location GetPointFromAddress(string address)
        {
            if (address == "")
            {
                throw new ArgumentNullException();
            }
            return BingMapsRestService.GetLocationFromAddress(address + " Danmark");
        }

        private void ClearRoute(object sender, RoutedEventArgs e)
        {
            _listOfLocationsOnRoute.Clear();
            _optimizedRoute.Clear();

            _addressField.Text = "";
            _displayedLocation.Text = "";

            _addressAsLocation = null;

            //Resets map
            _myMap.Children.Clear();
            _myMap.ZoomLevel = 6;
            _myMap.Center = new Location
            {
                Latitude = 55.901785,
                Longitude = 11.26392
            };

            _optimizedLengthField.Text = "Samlet distance: ";
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            Environment.Exit(1);
        }

        private class OptimizedRouteName
        {
            public string Name { get; set; }
        }

    }
}
