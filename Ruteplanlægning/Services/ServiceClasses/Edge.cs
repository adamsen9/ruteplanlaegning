﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ruteplanlægning.Services.ServiceClasses
{
    public class Edge
    {
        public Node Source { get; set; }
        public Node End { get; set; }
        public double Weight { get; set; }
    }
}
