﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services.Implementations
{
    public static class SharedMethods
    {
        public static double TourLength(Graph graph, List<int> tour)
        {
            var weight = 0.0;
            for (var i = 0; i < tour.Count - 1; i++)
            {
                var node = graph.Nodes.FirstOrDefault(x => x.Id == tour[i]);
                var nextNode = graph.Nodes.FirstOrDefault(x => x.Id == tour[i + 1]);

                weight += graph.Edges.FirstOrDefault(x =>
                    (x.Source == node || x.End == node) && (x.End == nextNode || x.Source == nextNode)).Weight;
            }



            return weight;
        }

        /// <summary>
        /// Returns a tour as a list of the edges that tour passes through.
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="tour"></param>
        /// <returns></returns>
        public static List<Edge> TourAsEdges(Graph graph, List<int> tour)
        {
            var toReturn = new List<Edge>();

            for (var i = 0; i < tour.Count - 1; i++)
            {
                var vode = graph.Nodes.FirstOrDefault(x => x.Id == tour[i]);
                var nextNode = graph.Nodes.FirstOrDefault(x => x.Id == tour[i + 1]);

                toReturn.Add(graph.Edges.FirstOrDefault(x =>
                    (x.Source == vode || x.End == vode) && (x.End == nextNode || x.Source == vode)));
            }

            return toReturn;
        }


        /// <summary>
        /// Shifts all elements in a list right
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lst"></param>
        /// <param name="shifts"></param>
        public static void ShiftRight<T>(List<T> lst, int shifts) // Tyvstjålet herfra:
        {
            for (var i = lst.Count - shifts - 1; i >= 0; i--)
            {
                lst[i + shifts] = lst[i];
            }

            for (var i = 0; i < shifts; i++)
            {
                lst[i] = default(T);
            }
        }
    }
}
