﻿namespace Ruteplanlægning
{
    public class RouteStopObject
    {
        public string Name { get; set; }
        public double Lattitude { get; set; }
        public double Longitude { get; set; }
        public bool? IsHome { get; set; }
    }
}
