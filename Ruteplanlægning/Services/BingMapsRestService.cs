﻿using Microsoft.Maps.MapControl.WPF;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Ruteplanlægning.Services
{
    public static class BingMapsRestService
    {
        public static Location GetLocationFromAddress(string address)
        {
            //throw new NotImplementedException();

            var client = new RestClient("http://dev.virtualearth.net/REST/v1/Locations/");
            var request = new RestRequest(address, Method.GET);
            request.AddParameter("key", "AmzRoPDlDkFhfqlH8ymOeI-ayB9CgxjJRPwj5r0vFhKS7O8-Xn4wXmf8BPq2wlyp");
            request.AddParameter("countryRegion", "denmark");

            var response = client.Execute(request);

            dynamic values = JObject.Parse(response.Content);

            var latitude = values["resourceSets"][0]["resources"][0]["point"]["coordinates"][0];
            var longitude = values["resourceSets"][0]["resources"][0]["point"]["coordinates"][1];
            var location = new Location
            {
                Latitude = (double)latitude,
                Longitude = (double)longitude
            };

            return location;
        }
    }
}