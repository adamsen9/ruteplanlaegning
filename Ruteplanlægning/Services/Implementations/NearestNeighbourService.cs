﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services.Implementations
{
    public class NearestNeighbourService : IAlgorithmService
    {
        private static Node _home;

        public List<int> RunAlgorithm(Graph graph)
        {
            var copyOfNodes = graph.Nodes.ToList();
            var copyOfEdges = graph.Edges.ToList();

            var current = copyOfNodes[0];
            _home = current;
            copyOfNodes.Remove(current);

            var initialList = new List<int> { _home.Id };

            var result = Function(initialList, current, copyOfNodes, copyOfEdges);

            return result;
        }

        //Recursive function
        private static List<int> Function(List<int> listToReturn, Node current, List<Node> nodes, List<Edge> edges)
        {
            if (nodes.Any())
            {
                var neighbourEdges = edges.Where(x => x.Source == current || x.End == current).ToList();
                var smallestV = neighbourEdges.OrderBy(x => x.Weight).FirstOrDefault();

                var closestsN = smallestV.Source == current ? smallestV.End : smallestV.Source;

                foreach (var neighbourEdge in neighbourEdges)
                {
                    edges.Remove(neighbourEdge);
                }

                nodes.Remove(closestsN);

                listToReturn.Add(closestsN.Id);

                Function(listToReturn, closestsN, nodes, edges);
            }
            else
            {
                //empty list, adding home as final destination
                listToReturn.Add(_home.Id);
            }
            return listToReturn;
        }
    }
}
