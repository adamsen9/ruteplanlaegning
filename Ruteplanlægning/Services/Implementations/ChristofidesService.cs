﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using RestSharp;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services.Implementations
{
    public class ChristofidesService : IAlgorithmService
    {
        public List<int> RunAlgorithm(Graph graph)
        {
            var copyOfNodes = graph.Nodes.ToList();
            var copyOfEdges = graph.Edges.ToList();

            // Calculate minimum spanning tree T (kruskals)

            // Start with smallest edge
            var edgesInT = new List<Edge>();
            var nodesInT = new List<Node>();

            var orderedEdges = copyOfEdges.OrderBy(x => x.Weight).ToList();

            var smallestEdge = orderedEdges.FirstOrDefault();

            edgesInT.Add(smallestEdge);

            orderedEdges.Remove(smallestEdge);

            nodesInT.Add(smallestEdge.Source);
            nodesInT.Add(smallestEdge.End);

            copyOfNodes.Remove(smallestEdge.Source);
            copyOfNodes.Remove(smallestEdge.End);

            while (orderedEdges.Any())
            {
                //repeadetly find the smallest edge that does not create a cycle
                var newSmallestEdge = orderedEdges.FirstOrDefault();
                orderedEdges.Remove(newSmallestEdge);

                if (!nodesInT.Contains(newSmallestEdge.Source) || !nodesInT.Contains(newSmallestEdge.End))
                {
                    edgesInT.Add(newSmallestEdge);

                    nodesInT.Add(newSmallestEdge.Source);
                    nodesInT.Add(newSmallestEdge.End);

                    copyOfNodes.Remove(smallestEdge.Source);
                    copyOfNodes.Remove(smallestEdge.End);
                }
            }

            nodesInT = nodesInT.Distinct().ToList();

            // Set of nodes O with odd degree in 

            var oddDegreeNodes = new List<Node>();
            foreach (var node in nodesInT)
            {
                int degree = 0;
                foreach (var edge in edgesInT)
                {
                    if (edge.Source == node || edge.End == node)
                    {
                        degree++;
                    }
                }
                if (degree % 2 != 0) //Degree must be odd
                {
                    oddDegreeNodes.Add(node);
                }
            }

            // Form subgraph of G using only the vertices of O
            var subGraph = new Graph { Nodes = oddDegreeNodes };

            foreach (var edge in copyOfEdges)
            {
                if (subGraph.Nodes.Contains(edge.Source) && subGraph.Nodes.Contains(edge.End))
                {
                    subGraph.Edges.Add(edge);
                }
            }

            // Construct a minimum-weight perfect matching M in this subgraph

            var subEdgesOrdered = subGraph.Edges.OrderBy(x => x.Weight).ToList();

            var m = new Graph();

            while (subEdgesOrdered.Any())
            {
                var smallestSubEdge = subEdgesOrdered.FirstOrDefault();
                subEdgesOrdered.Remove(smallestSubEdge);

                m.Edges.Add(smallestSubEdge);
                m.Nodes.Add(smallestSubEdge.Source);
                m.Nodes.Add(smallestSubEdge.End);
                //Remove all edges related to source and end
                subEdgesOrdered.RemoveAll(x => x.Source == smallestSubEdge.Source || x.Source == smallestSubEdge.End || x.End == smallestSubEdge.Source || x.End == smallestSubEdge.End);
            }

            // Unite matching and spanning tree T and M to form an eulerian multigraph

            m.Edges.AddRange(edgesInT);
            m.Edges = m.Edges.Distinct().ToList();
            m.Nodes.AddRange(nodesInT.ToList());
            m.Nodes = m.Nodes.Distinct().ToList();
            // Calculate euler tour

            // find starting vertex
            //if any node has an uneven degree, start at any one of these.
            // else, start at the one with id 0 (eg. home).

            var startingNode = m.Nodes.FirstOrDefault(x => x.Id == 0);
            foreach (var node in m.Nodes)
            {
                var degree = m.Edges.Count(edge => edge.Source == node || edge.End == node);

                if (degree % 2 != 0)
                {
                    startingNode = node;
                    break;
                }
            }

            var eulerTour = new List<int> { startingNode.Id };

            //listing all neighbour nodes
            var currentNode = startingNode;
            currentNode.Flag = true;

            while (m.Nodes.Any(x => x.Flag != true)) //Flag is used to indicate if a node has been visited, eg. the loop continues while there are nodes that are unvisited.
            {
                var neighbourEdges = m.Edges.Where(x => x.Source == currentNode || x.End == currentNode);

                //Find a viable edge
                foreach (var edge in neighbourEdges)
                {
                    if (IsValidNextEdge(currentNode, edge, m))
                    {
                        currentNode = edge.Source == currentNode ? edge.End : edge.Source;
                        currentNode.Flag = true;
                        m.Edges.Remove(edge);
                        break;
                        //Edge is a valid path
                    }
                }
                eulerTour.Add(currentNode.Id);
            }

            eulerTour = eulerTour.Distinct().ToList();

            eulerTour.Add(startingNode.Id);

            return eulerTour;
        }

        private bool IsValidNextEdge(Node node, Edge edge, Graph graph)
        {
            //The edge is valid in two cases:
            //1) If the edge is the only adjacent edge to node
            if (graph.Edges.Count(x => x.Source == node || x.End == node) == 1)
            {
                return true;
            }

            //2) If there are multiple adjacent edges, then the edge is not a bridge
            // Steps to check

            // 1. Count reachable nodes from the node 
            var count1 = DFSCount(node, graph);

            // 2. Remove the edge, and after removing the edge, count the rechable nodes from the node
            var count2 = DFSCount(node, new Graph
            {
                Edges = graph.Edges.Where(x => x != edge).ToList(),
                Nodes = graph.Nodes
            });

            // 2. If count1 if greater than count2, then the edge is a bridge, and is invalid
            return count1 <= count2;
        }

        private int DFSCount(Node node, Graph graph)
        {
            //Resetting secondary flags
            foreach (var graphNode in graph.Nodes)
            {
                graphNode.SecondaryFlag = false;
            }

            var count = 0;
            var S = new Stack<Node>();
            S.Push(node);
            while (S.Any())
            {
                var v = S.Pop();
                if (!v.SecondaryFlag)
                {
                    v.SecondaryFlag = true;
                    count++;
                    foreach (var edge in graph.Edges)
                    {
                        if (edge.Source == v || edge.End == v)
                        {

                            if (edge.Source == v && !edge.End.SecondaryFlag)
                            {
                                S.Push(edge.End);
                            }

                            if (edge.End == v && !edge.Source.SecondaryFlag)
                            {
                                S.Push(edge.Source);
                            }
                        }
                    }
                }
            }

            return count;
        }
    }
}
