﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Ruteplanlægning.Model;
using Ruteplanlægning.Services.Implementations;
using Ruteplanlægning.Services.ServiceClasses;

namespace Ruteplanlægning.Services
{
    public static class RouteOptimizationService
    {
        public static ObservableCollection<RouteStopObject> OptimizeRoute(ObservableCollection<RouteStopObject> unorderedRoute, AlgorithmEnum pickedAlgorithm, out double length)
        {
            //Building dictionary with id
            var stopsAsDictionary = new Dictionary<int, RouteStopObject>();
            for (var i = 0; i < unorderedRoute.Count; i++)
            {
                stopsAsDictionary[i] = unorderedRoute[i];
            }

            //setting stop 0 as the home

            if (stopsAsDictionary.Keys.Count == 0)
            {
                throw new ArgumentNullException(nameof(unorderedRoute));
            }

            var isSet = false;

            if (!stopsAsDictionary[0].IsHome.HasValue)
            {
                //0 is not home, finding home and swapping the objects
                foreach (var key in stopsAsDictionary.Keys.ToList())
                {
                    if (stopsAsDictionary[key].IsHome.HasValue)
                    {
                        //found it, putting 0 in temp
                        var temp = stopsAsDictionary[0];
                        stopsAsDictionary[0] = stopsAsDictionary[key];
                        stopsAsDictionary[key] = temp;
                        isSet = true;
                    }
                }
            }
            else
            {
                isSet = true;
            }

            if (!isSet)
            {
                //TODO: Assuming stop no 1 is home
            }

            // Setting up graph
            var graph = new Graph();

            // Convert list to nodes
            foreach (var key in stopsAsDictionary.Keys)
            {
                graph.Nodes.Add(new Node(key)
                {
                    Name = stopsAsDictionary[key].Name,
                });
            }

            // Create edges between all nodes
            for (var i = 0; i < unorderedRoute.Count; i++)
            {
                var source = graph.Nodes.FirstOrDefault(x => x.Id == i);
                for (var j = i + 1; j < unorderedRoute.Count; j++)
                {
                    var end = graph.Nodes.FirstOrDefault(x => x.Id == j);
                    var edge = new Edge
                    {
                        End = end,
                        Source = source,
                        Weight = DistanceBetweenRouteStopObjects(stopsAsDictionary[j], stopsAsDictionary[i])
                    };

                    graph.Edges.Add(edge);
                }
            }

            // Switching on what algorithm to use
            IAlgorithmService service;
            switch (pickedAlgorithm)
            {
                case AlgorithmEnum.Nna:
                    service = new NearestNeighbourService();
                    break;
                case AlgorithmEnum.TwoOpt:
                    service = new TwoOptSwapService();
                    break;
                case AlgorithmEnum.BruteForce:
                    service = new BruteForceService();
                    break;
                case AlgorithmEnum.Christofides:
                    service = new ChristofidesService();
                    break;
                default:
                    service = new NearestNeighbourService();
                    break;
            }

            // Running algorithm
            var orderedResultIds = service.RunAlgorithm(graph);

            var toReturn = new ObservableCollection<RouteStopObject>();
            foreach (var id in orderedResultIds)
            {
                toReturn.Add(stopsAsDictionary[id]);
            }

            length = SharedMethods.TourLength(graph, orderedResultIds);

            return toReturn;
        }


        /// <summary>
        /// Haversine formula for finding the distance between two points on a sphere given lattitude and longtitude
        /// </summary>
        /// <param name="stopOne"></param>
        /// <param name="stopTwo"></param>
        /// <returns></returns>
        private static double DistanceBetweenRouteStopObjects(RouteStopObject stopOne, RouteStopObject stopTwo)
        {
            const int r = 6371000; //Earths mean radius

            var lat1 = Deg2Rad(stopOne.Lattitude);
            var lon1 = Deg2Rad(stopOne.Longitude);

            var lat2 = Deg2Rad(stopTwo.Lattitude);
            var lon2 = Deg2Rad(stopTwo.Longitude);

            var phi1 = lat1;
            var phi2 = lat2;

            var deltaPhi = lat1 - lat2;
            var deltaLambda = lon1 - lon2;

            var a = Math.Sin(deltaPhi / 2.0) * Math.Sin(deltaPhi / 2) +
                    Math.Cos(phi1) * Math.Cos(phi2) *
                    Math.Sin(deltaLambda / 2) * Math.Sin(deltaLambda / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            var d = r * c;

            return d;
        }

        /// <summary>
        /// Converts degrees to radians
        /// </summary>
        /// <param name="deg"></param>
        /// <returns></returns>
        private static double Deg2Rad(double deg)
        {
            return deg * Math.PI / 180.0;
        }
    }
}